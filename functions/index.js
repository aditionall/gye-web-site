const functions = require("firebase-functions");
const nodemailer = require("nodemailer");
'teste-deploy-functions'

var emailNotification = "dev-edd";
// const emailNotification = "null";

if (emailNotification === null) {
  emailNotification = "hello@gyeproyecto.com";
} else {
  emailNotification = "edd.innovation@relik.cool";
}

const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;

const transport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

exports.sendEmailCont = functions.firestore
  .document("formContact/{id}")
  .onCreate((snap, context) => {
    const email = snap.data().email;
    const name = snap.data().name;
    const phone = snap.data().phone;
    const comment = snap.data().comment;
    return notifyInsertCont(email, name, phone, comment);
  });

function notifyInsertCont(email, name, phone, comment) {
  return transport.sendMail({
    from: "Gye Proyectos México <hello@gyeproyecto.com>",
    // to: "hello@gyeproyecto.com",
    to: emailNotification,
    subject:
      "Tienes un nuevo mensaje en la sección de Contacto de Web Gye Proyectos",
    html: `
    <h1 style="color:#FFBD27">GYE PROYECTOS MÉXICO</h1>
    <p>Tienes un nuevo mensaje en la sección de Contacto de Web Gye Proyectos</p>
    <br/>
    <tr><td>Nombre</td><td>${name}</td></tr>
    <tr><td>Correo Electrónico</td><td>${email}</td></tr>
    <tr><td>Teléfono</td><td>${phone}</td></tr>
    <tr><td>Comentario</td><td>${comment}</td></tr>
    <br/>
    <p>Ponte en contacto cuanto antes.</p>
    `,
  });
}

exports.sendEmailInstr = functions.firestore
  .document("formInstr/{id}")
  .onCreate((snap, context) => {
    const name = snap.data().name;
    const position = snap.data().position;
    const company = snap.data().company;
    const phone = snap.data().phone;
    const email = snap.data().email;
    const sector = snap.data().sector;
    const project = snap.data().project;
    const adress = snap.data().adress;
    const application = snap.data().application;

    const otherApplication = snap.data().otherApplication;
    const measureHumidity = snap.data().measureHumidity;
    const measureTemperature = snap.data().measureTemperature;
    const measureCo2 = snap.data().measureCo2;
    const measureDifPressure = snap.data().measureDifPressure;
    const measurePresion = snap.data().measurePresion;
    const measurewater = snap.data().measurewater;
    const measuredew = snap.data().measuredew;
    const measureO2 = snap.data().measureO2;

    const instrument = snap.data().instrument;
    const rangesToMeasure = snap.data().rangesToMeasure;
    const requireMinPress = snap.data().requireMinPress;
    const needMonitorSystems = snap.data().needMonitorSystems;
    return notifyInsertInstr(
      name,
      position,
      company,
      phone,
      email,
      sector,
      project,
      adress,
      application,
      otherApplication,
      measureHumidity,
      measureTemperature,
      measureCo2,
      measureDifPressure,
      measurePresion,
      measurewater,
      measuredew,
      measureO2,
      instrument,
      rangesToMeasure,
      requireMinPress,
      needMonitorSystems
    );
  });

function notifyInsertInstr(
  name,
  position,
  company,
  phone,
  email,
  sector,
  project,
  adress,
  application,
  otherApplication,
  measureHumidity,
  measureTemperature,
  measureCo2,
  measureDifPressure,
  measurePresion,
  measurewater,
  measuredew,
  measureO2,
  instrument,
  rangesToMeasure,
  requireMinPress,
  needMonitorSystems
) {
  return transport.sendMail({
    from: "Gye Proyectos México <hello@gyeproyecto.com>",
    // to: "hello@gyeproyecto.com",
    to: emailNotification,
    subject: "Tienes un nuevo mensaje de Web Gye Proyectos",
    text: "Un usuario ha dejado la siguiente información ",
    html: `
    <h1 style="color:#FFBD27">GYE PROYECTOS MÉXICO</h1>
    <p>Nueva Cotización Instrumentación Rotronic</p>
    <br/>
    <table >
    <tr><th style="color:#328CC1" >Información general</th></tr>
    <tr><td>Nombre</td><td>${name}</td></tr>
    <tr><td>Cargo</td><td>${position}</td></tr>
    <tr><td>Empresa</td><td>${company}</td></tr>
    <tr><td>Teléfono</td><td>${phone}</td></tr>
    <tr><td>Correo Electrónico</td><td>${email}</td></tr>
    <tr><td>Sector</td><td>${sector}</td></tr>
    <tr><td>Proyecto</td><td>${project}</td></tr>
    <tr><td>Dirección </td><td>${adress}</td></tr>
    <tr><th style="color:#328CC1">¿Para que aplicación es?</th></tr>
    <tr><td style="text-align: center;">${application}</td></tr>
    <tr><td>Especifique</td><td>${otherApplication}</td></tr>
    <tr><th style="color:#328CC1">¿Que quieres medir?</th></tr>
    <tr><td>Humedad</td><td>${measureHumidity}</td></tr>
    <tr><td>Temperatura</td><td>${measureTemperature}</td></tr>
    <tr><td>Co2</td><td>${measureCo2}</td></tr>
    <tr><td>Presion diferencial</td><td>${measureDifPressure}</td></tr>
    <tr><td>Presión</td><td>${measurePresion}</td></tr>
    <tr><td>Agua</td><td>${measurewater}</td></tr>
    <tr><td>Punto de rocío </td><td>${measuredew}</td></tr>
    <tr><td>O2</td><td>${measureO2}</td></tr>
    <tr><th style="color:#328CC1">¿Que instrumento necesitas?</th></tr>
    <tr><td style="text-align: center;">${instrument}</td></tr>
    <tr><td >QUE RANGOS QUIERES MEDIR?</td><td>${rangesToMeasure}</td></tr>
    <tr><td>REQUIERES ALGUNA PRESICIÓN MINIMA? CUAL?</td><td>${requireMinPress}</td></tr>
    <tr><td>NECESITAS SISTEMA DE MONITOREO CONTINUO Y REMOTO?</td><td>${needMonitorSystems}</td></tr>    
    </table>
    <br/>
    <p>Ponte en contacto cuanto antes.</p>
    `,
  });
}

exports.sendEmailHum = functions.firestore
  .document("formHum/{id}")
  .onCreate((snap, context) => {
    const name = snap.data().name;
    const position = snap.data().position;
    const company = snap.data().company;
    const phone = snap.data().phone;
    const email = snap.data().email;
    const sector = snap.data().sector;
    const project = snap.data().project;
    const adress = snap.data().adress;
    const application = snap.data().application;
    const otherApplication = snap.data().otherApplication;

    const typeElectric = snap.data().typeElectric;
    const typeGas = snap.data().typeGas;
    const typeVapor = snap.data().typeVapor;
    const typeadiabatic = snap.data().typeadiabatic;
    const othertype = snap.data().othertype;

    const dimensionDuct = snap.data().dimensionDuct;
    const measureDuct = snap.data().measureDuct;
    const dimensionUma = snap.data().dimensionUma;
    const measureUma = snap.data().measureUma;

    const typeWater = snap.data().typeWater;
    const city = snap.data().city;
    const outdoorConditions = snap.data().outdoorConditions;
    const conditionsReach = snap.data().conditionsReach;
    const needAutomat = snap.data().needAutomat;
    const needComunication = snap.data().needComunication;

    return notifyInsertHum(
      name,
      position,
      company,
      phone,
      email,
      sector,
      project,
      adress,
      application,
      otherApplication,
      typeElectric,
      typeGas,
      typeVapor,
      typeadiabatic,
      othertype,
      dimensionDuct,
      measureDuct,
      dimensionUma,
      measureUma,
      typeWater,
      city,
      outdoorConditions,
      conditionsReach,
      needAutomat,
      needComunication
    );
  });

function notifyInsertHum(
  name,
  position,
  company,
  phone,
  email,
  sector,
  project,
  adress,
  application,
  otherApplication,

  typeElectric,
  typeGas,
  typeVapor,
  typeadiabatic,
  othertype,
  dimensionDuct,
  measureDuct,
  dimensionUma,
  measureUma,

  typeWater,
  city,
  outdoorConditions,
  conditionsReach,
  needAutomat,
  needComunication
) {
  return transport.sendMail({
    from: "Gye Proyectos México <hello@gyeproyectos.com>",
    // to: "hello@gyeproyecto.com",
    to: emailNotification,
    subject: "Tienes un nuevo mensaje de Web Gye Proyectos",
    text: "Un usuario ha dejado la siguiente información ",
    html: `
    <h1 style="color:#FFBD27">GYE PROYECTOS MÉXICO</h1>
    <p>Nueva Cotización Humidificadores</p>
    <br/>
    <table >
    <tr><th style="color:#328CC1" >Información general</th></tr>
    <tr><td>Nombre</td><td>${name}</td></tr>
    <tr><td>Cargo</td><td>${position}</td></tr>
    <tr><td>Empresa</td><td>${company}</td></tr>
    <tr><td>Teléfono</td><td>${phone}</td></tr>
    <tr><td>Correo Electrónico</td><td>${email}</td></tr>
    <tr><td>Sector</td><td>${sector}</td></tr>
    <tr><td>Proyecto</td><td>${project}</td></tr>
    <tr><td>Dirección </td><td>${adress}</td></tr>
    <tr><th style="color:#328CC1">¿Para que aplicación es?</th></tr>
    <tr><td style=" text-align: center;">${application}</td></tr>
    <tr><td>Especifique</td><td>${otherApplication}</td></tr>
    <tr><th style="color:#328CC1">¿Qué tipo de humidificador estás buscando?</th></tr>
    <tr><td>Eléctrico</td><td>${typeElectric}</td></tr>
    <tr><td>Gas</td><td>${typeGas}</td></tr>
    <tr><td>Vapor</td><td>${typeVapor}</td></tr>
    <tr><td>Adiabático</td><td>${typeadiabatic}</td></tr>
    <tr><td>¿Otro?</td><td>${othertype}</td></tr>
    <tr><th style="color:#328CC1">¿Dónde colocamos el dispersor?</th></tr>
    <tr><td>Ducto ¿Tienes las dimensiones?</td><td>${dimensionDuct}</td></tr>
    <tr><td>¿Qué dimensiones tiene?</td><td>${measureDuct}</td></tr>
    <tr><td>UMA ¿Tienes las dimensiones?</td><td>${dimensionUma}</td></tr>
    <tr><td>¿Qué dimensiones tiene?</td><td>${measureUma}</td></tr>
    <tr><th style="color:#328CC1">¿Qué tipo de agua usarás?</th></tr>
    <tr><td style=" text-align: center;">${typeWater}</td></tr>
    <tr><td>¿Ciudad donde lo instalarás?</td><td>${city}</td></tr>
    <tr><td>Condiciones de temperatura y humedad exterior</td><td>${outdoorConditions}</td></tr>
    <tr><td>Condiciones que desea alcanzar</td><td>${conditionsReach}</td></tr>
    <tr><td>¿Necesitas automatización?</td><td>${needAutomat}</td></tr>
    <tr><td>¿Requiere comunicación de control?</td><td>${needComunication}</td></tr>
    </table>
    <br/>
    <p>Ponte en contacto cuanto antes.</p>
    `,
  });
}

exports.sendEmailBms = functions.firestore
  .document("formBms/{id}")
  .onCreate((snap, context) => {
    const name = snap.data().name;
    const position = snap.data().position;
    const company = snap.data().company;
    const phone = snap.data().phone;
    const email = snap.data().email;
    const sector = snap.data().sector;
    const project = snap.data().project;
    const adress = snap.data().adress;
    const application = snap.data().application;
    const otherApplication = snap.data().otherApplication;
    const whatwant = snap.data().whatwant;

    const paramTemp = snap.data().paramTemp;
    const paramHumi = snap.data().paramHumi;
    const paramDifPres = snap.data().paramDifPres;
    const paramFlow = snap.data().paramFlow;
    const paramDioxide = snap.data().paramDioxide;
    const otherparam = snap.data().otherparam;

    const equipAirHand = snap.data().equipAirHand;
    const equipExtrac = snap.data().equipExtrac;
    const equipFilter = snap.data().equipFilter;
    const equipCollec = snap.data().equipCollec;
    const equipExpan = snap.data().equipExpan;
    const equipHumidifier = snap.data().equipHumidifier;
    const equipDehum = snap.data().equipDehum;
    const equipChillers = snap.data().equipChillers;
    const equipCool = snap.data().equipCool;
    const equipDoor = snap.data().equipDoor;
    const equipBombs = snap.data().equipBombs;
    const equipOther = snap.data().equipOther;

    return notifyInsertBms(
      name,
      position,
      company,
      phone,
      email,
      sector,
      project,
      adress,
      application,
      otherApplication,
      whatwant,

      paramTemp,
      paramHumi,
      paramDifPres,
      paramFlow,
      paramDioxide,
      otherparam,

      equipAirHand,
      equipExtrac,
      equipFilter,
      equipCollec,
      equipExpan,
      equipHumidifier,
      equipDehum,
      equipChillers,
      equipCool,
      equipDoor,
      equipBombs,
      equipOther
    );
  });

function notifyInsertBms(
  name,
  position,
  company,
  phone,
  email,
  sector,
  project,
  adress,
  application,
  otherApplication,
  whatwant,
  paramTemp,
  paramHumi,
  paramDifPres,
  paramFlow,
  paramDioxide,
  otherparam,
  equipAirHand,
  equipExtrac,
  equipFilter,
  equipCollec,
  equipExpan,
  equipHumidifier,
  equipDehum,
  equipChillers,
  equipCool,
  equipDoor,
  equipBombs,
  equipOther
) {
  return transport.sendMail({
    from: "Gye Proyectos México <hello@gyeproyectos.com>",
    // to: "hello@gyeproyecto.com",
    to: emailNotification,
    subject: "Tienes un nuevo mensaje de Web Gye Proyectos",
    text: "Un usuario ha dejado la siguiente información ",
    html: `
    <h1 style="color:#FFBD27">GYE PROYECTOS MÉXICO</h1>
    <p>Nueva Cotización BMS</p>
    <br/>
    <table >
    <tr><th style="color:#328CC1" >Información general</th></tr>
    <tr><td>Nombre</td><td>${name}</td></tr>
    <tr><td>Cargo</td><td>${position}</td></tr>
    <tr><td>Empresa</td><td>${company}</td></tr>
    <tr><td>Teléfono</td><td>${phone}</td></tr>
    <tr><td>Correo Electrónico</td><td>${email}</td></tr>
    <tr><td>Sector</td><td>${sector}</td></tr>
    <tr><td>Proyecto</td><td>${project}</td></tr>
    <tr><td>Dirección </td><td>${adress}</td></tr>
    <tr><th style="color:#328CC1">¿Para que aplicación es?</th></tr>
    <tr><td style=" text-align: center;">${application}</td></tr>
    <tr><td>¿Otro? Especifique</td><td>${otherApplication}</td></tr>
    <tr><th style="color:#328CC1"">¿Que quieres?</th></tr>
    <tr><td style=" text-align: center;">${whatwant}</td></tr>
    <tr><th style="color:#328CC1">¿Que parametros?</th></tr>
    <tr><td>Temperatura</td><td>${paramTemp}</td></tr>
    <tr><td>Humedad</td><td>${paramHumi}</td></tr>
    <tr><td>Presión diferencial</td><td>${paramDifPres}</td></tr>
    <tr><td>Flujo</td><td>${paramFlow}</td></tr>
    <tr><td>Dióxido de carbono</td><td>${paramDioxide}</td></tr>
    <tr><td>¿Otro? Especifique</td><td>${otherparam}</td></tr>
    <tr><th style="color:#328CC1">¿Que equipos tienes?</th></tr>
    <tr><td>Unidades de tratamiento de aire</td><td>${equipAirHand}</td></tr>
    <tr><td>Extractores</td><td>${equipExtrac}</td></tr>
    <tr><td>Bancos de filtros</td><td>${equipFilter}</td></tr>
    <tr><td>Colectores de polvo</td><td>${equipCollec}</td></tr>
    <tr><td>Unidades de expansión directa</td><td>${equipExpan}</td></tr>
    <tr><td>Humidificadores</td><td>${equipHumidifier}</td></tr>
    <tr><td>Deshumidificadores</td><td>${equipDehum}</td></tr>
    <tr><td>Chillers</td><td>${equipChillers}</td></tr>
    <tr><td>Torres de enfriamiento</td><td>${equipCool}</td></tr>
    <tr><td>Compuertas</td><td>${equipDoor}</td></tr>
    <tr><td>Bombas</td><td>${equipBombs}</td></tr>
    <tr><td>¿Otro?</td><td>${equipOther}</td></tr>
    </table>
    <br/>
    <p>Ponte en contacto cuanto antes.</p>
    `,
  });
}
