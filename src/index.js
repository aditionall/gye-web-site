import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
//import 'font-awesome/css/font-awesome.min.css';
import '../src/assets/styles/style.scss'
import {Provider} from 'react-redux';
import { createStore } from 'redux';
import reducer from './reducers'
import {I18nextProvider} from "react-i18next";
import i18next from 'i18next';
import global_en from './translations/en/global.json';
import global_es from './translations/es/global.json';
import './firebase/firebase-config';

i18next.init({
  interpolation: {escapeValue: false},
  lng: "es",
  resources: {
    es: {
      global : global_es,
    },
    en: {
      global : global_en,
    },
  },
});

const store = createStore(reducer)

ReactDOM.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18next}>
    <Provider store = {store}>
      <App />
    </Provider>
    </I18nextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
