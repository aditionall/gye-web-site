import React , { useState }  from 'react';
import logo from "../assets/static/logo-light.png";
import logoTitle from "../assets/static/logo-title.png";
import menuIcon from "../assets/static/menu-normal.png";
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';

 function Header(props){

  const bgHeader = (props.bg == null) ? "header--solid" : "header--"+props.bg

  const [t, i18n] = useTranslation("global");
  let lang = t("general.lang")

  const [open, setOpen] = useState(false)

  /*const handleControlMenu = (isOpen) =>{
    setOpen(true)
  }*/

  const handleLang = () =>{
    i18n.changeLanguage(lang)
  }

  return (
    <header className={"header "+ bgHeader}>

      <div className="header__container">
        <div className="header__brand" >
          
          <Link to="/" className="brand__container" onClick={() => setOpen(true)}>
            <img className="brand__minify" src={logo} />
            <img className="brand__text" src={logoTitle}/>
          </Link>

        </div>        

        <img className ={"menu__image menu__image" + ((open)? '--open':'--close')} src={menuIcon} onClick={() => setOpen(!open)}/>

        <div className='header__lang' onClick={() => handleLang() }><a > {t("general.lang")} </a></div>    
      </div>

      <div className="header__menu">
        <ul className={"menu__options menu__options" + ((open) ?'--open':'--close')}>
          
          <Link to="/Services" onClick={() => setOpen(false)}><li className='option__services'>{t("general.services")}</li></Link>
          <a href="https://www.controlito.one/" target="_blank" onClick={() => setOpen(false)}><li className='option__products'>{t("Productos")}</li></a>          
          <Link to="/Quote" onClick={() => setOpen(false)}><li className='option__quotes'>{t("general.proposal")}</li></Link>          
          <Link to="/Contact" onClick={() => setOpen(false)}><li className='option__contact'>{t("general.contact")}</li></Link>
          <a onClick={() => handleLang()}><li className='option__lang'>{t("general.lang")}</li></a>
            
        </ul>
      </div>

    </header>
  );
}



export default Header;