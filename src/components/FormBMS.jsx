import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import { useForm, Controller } from 'react-hook-form';
import { db } from '../firebase/firebase-config';
import Swal from 'sweetalert2'

function FormBMS() {
    var hoy = new Date();
    var fecha = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear();
    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();

    const [t, i18n] = useTranslation("global")
    const { register, errors, handleSubmit } = useForm();
    const [Entradas, setEntradas] = useState('')

    const insertFormBms = (linkObject) => {
        db.collection('formBms').doc().set(linkObject)
        console.log('OK BMS' + fecha + " " + hora);
        Enviado();
    }

    const onSubmit = (data, e) => {
        setEntradas([...Entradas, data])
        insertFormBms(data);
        e.target.reset();
    };

    const Other = () => {
        var idOther = document.getElementById("idOtherBms");
        var other = document.getElementById("applicationBms").value;
        console.log(other);
        if (other === "otro") {
            idOther.style.display = "block";
        } else {
            idOther.style.display = "none";
        }
    }

    const Enviado = ()=>{
        Swal.fire({
            title: t("alert.tanks"),
            text: t("alert.msjcontact"),
            icon:"success"
        }
          )
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} className="container-form" >
                <input name="date" type="hidden" value={fecha + " " + hora} ref={register} />
                <input name="form" type="hidden" value="Bms" ref={register} />
                <input name="idform" type="hidden" value="formBms" ref={register} />
                <input name="status" type="hidden" value="" ref={register} />
                <input name="assigned" type="hidden" value=""ref={register} />
                <input name="sellercomments" type="hidden" value=""ref={register} />
                <input name="admincomments" type="hidden" value=""ref={register} />
                
                <h4>{t("gral_data.title")}</h4>
                <br/>
                <div className="row" >
                    <div className="col-sm">
                        <input name="name" placeholder={t("gral_data.name")} className="form-control" type="text" ref={register({ required: true, pattern: /[A-Za-z ñ]+/ })} />
                        <div className="text-danger text-small d-block mb-2">{errors.name && t("gral_data.msjname")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="position" placeholder={t("gral_data.position")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="company" placeholder={t("gral_data.company")} className="form-control" type="text" ref={register({ required: true })} />
                        <div className="text-danger text-small d-block mb-2">{errors.company && t("gral_data.msjcompany")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="phone" placeholder={t("gral_data.phone")} className="form-control" type="tel" ref={register({
                            required: true, maxLength: 22, minLength: 7, pattern: /^[0-9]+/
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.phone && t("gral_data.msjphone")}</div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="email" placeholder={t("gral_data.email")} className="form-control" type="text" ref={register({
                            required: true,
                            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.email && t("gral_data.msjemail")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="sector" placeholder={t("gral_data.sector")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="project" placeholder={t("gral_data.project")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        <input name="adress" placeholder={t("gral_data.adress")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <h4>{t("gral_data.application")}</h4>
                        <div >
                        <select id="applicationBms" className="form-control" name="application" ref={register} onChange={Other}>
                                <option className="form-control" value="">{t("gral_data.default")}</option>
                                <option className="form-control" value="Farmacéutica">{t("gral_data.pharma")}</option>
                                <option className="form-control" value="Hospitalaria">{t("gral_data.hosp")}</option>
                                <option className="form-control" value="HVAC">{t("gral_data.hvac")}</option>
                                <option className="form-control" value="otro">{t("gral_data.other")}</option>
                            </select>
                        </div>
                        <br />
                        <input id="idOtherBms" name="otro" style={{ display: "none" }} placeholder={t("gral_data.specify")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="col-sm">
                        <h4>{t("formbms.require")}</h4>
                        <div>
                            <select className="form-control" name="whatwant" ref={register}>
                                <option className="form-control" >{t("gral_data.default")}</option>
                                <option className="form-control" value="Controlar">{t("formbms.control")}</option>
                                <option className="form-control" value="Monitorear">{t("formbms.monitor")}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br />
                <div>
                    <h4>{t("formbms.param")}</h4>
                    <div className="row">
                        <div className="form-group col-md" >
                            <label htmlFor="temp">{t("formbms.temp")}</label>
                            <input id="temp" className="form-control" type="checkbox" name="paramTemp" value="Yes" ref={register}></input>
                        </div>
                        <div className="form-group col-md">
                            <label htmlFor="humedad">{t("formbms.humidity")}</label>
                            <input id="humedad" className="form-control" type="checkbox" name="paramHumi" value="Yes" ref={register}></input>
                        </div>
                        <div className="form-group col-md">
                            <label htmlFor="difpres">{t("formbms.pressure")}</label>
                            <input id="difpres" className="form-control" type="checkbox" name="paramDifPres" value="Yes" ref={register}></input>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-md" >
                            <label htmlFor="flow">{t("formbms.flow")}</label>
                            <input id="flow" className="form-control" type="checkbox" name="paramFlow" value="Yes" ref={register}></input>
                        </div>
                        <div className="form-group col-md" >
                            <label htmlFor="diox">{t("formbms.dioxide")}</label>
                            <input id="diox" className="form-control" type="checkbox" name="paramDioxide" value="Yes" ref={register}></input>
                        </div>
                        <div className="form-group col-md" >
                            <label htmlFor="otro">{t("gral_data.other")}</label>
                            <input id="otro" name="otherparam" placeholder={t("gral_data.specify")} className="form-control" type="text" ref={register} />
                        </div>
                    </div>
                </div>
                <br />
                <h4 className="form-group">{t("formbms.teams")}</h4>
                <br />
                <div className="row" >
                    <div className="form-group col-md">
                        {t("formbms.air")}
                        <input name="equipAirHand" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.extrac")}
                        <input name="equipExtrac" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.filter")}
                        <input name="equipFilter" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        {t("formbms.collec")}
                        <input name="equipCollec" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.expan")}
                        <input name="equipExpan" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.humidifier")}
                        <input name="equipHumidifier" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        {t("formbms.dehum")}
                        <input name="equipDehum" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.chillers")}
                        <input name="equipChillers" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.cool")}
                        <input name="equipCool" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        {t("formbms.door")}
                        <input name="equipDoor" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("formbms.bombs")}
                        <input name="equipBombs" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                        {t("gral_data.other")}
                        <input name="equipOther" placeholder={t("formbms.quant")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <button className="btn-send">{t("gral_data.send")}</button>
            </form>
        </>
    )
}

export default FormBMS;