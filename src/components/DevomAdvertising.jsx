import React from "react";

const DevomAdvertising = () => (
    <div className="adverstising">
        <p>
            Powered by <a target='_blank' href="https://www.linkedin.com/in/dev-omar-marin/">
            RELIK'
            </a>
        </p>
    </div>
)

export default DevomAdvertising;