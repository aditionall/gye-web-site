import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import { useForm, Controller } from 'react-hook-form';
import { db } from '../firebase/firebase-config';
import Swal from 'sweetalert2'

function FormHum() {
    var hoy = new Date();
    var fecha = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear();
    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();

    const [t, i18n] = useTranslation("global")
    const { register, errors, handleSubmit } = useForm();
    const [Entradas, setEntradas] = useState('')

    const hideDuct = () => {
        var idDuct = document.getElementById("idDuct");
        var dimensionDuct = document.getElementById("dimensionDuct").value;
        if (dimensionDuct === "Si") {
            idDuct.style.display = "block";
        } else {
            idDuct.style.display = "none";
        }
    }

    const hideUma = () => {
        var idUma = document.getElementById("idUma");
        var dimensionUma = document.getElementById("dimensionUma").value;
        if (dimensionUma === "Si") {
            idUma.style.display = "block";
        } else {
            idUma.style.display = "none";
        }
    }

    const InsertFormHum = (linkObject) => {
        db.collection('formHum').doc().set(linkObject)
        console.log('OK FormHum' + fecha + " " + hora);
        Enviado();
    }

    const onSubmit = (data, e) => {
        setEntradas([...Entradas, data])
        InsertFormHum(data);
        e.target.reset();
    };

    const Other = () => {
        var idOther = document.getElementById("idOtherHum");
        var other = document.getElementById("applicationHum").value;
        console.log(other);
        if (other === "otro") {
            idOther.style.display = "block";
        } else {
            idOther.style.display = "none";
        }
    }

    const Enviado = () => {
        Swal.fire({
            title: t("alert.tanks"),
            text: t("alert.msjcontact"),
            icon: "success"
        }
        )
    }

    return (
        <>
            <form className="container-form" onSubmit={handleSubmit(onSubmit)}>
                <input name="date" type="hidden" value={fecha + " " + hora} ref={register} />
                <input name="form" type="hidden" value="Humidificadores" ref={register} />
                <input name="idform" type="hidden" value="formHum" ref={register} />
                <input name="status" type="hidden" value="" ref={register} />
                <input name="assigned" type="hidden" value="" ref={register} />
                <input name="sellercomments" type="hidden" value="" ref={register} />
                <input name="admincomments" type="hidden" value="" ref={register} />
                <h4>{t("gral_data.title")}</h4>
                <br />
                <div className="row" >
                    <div className="col-sm">
                        <input name="name" placeholder={t("gral_data.name")} className="form-control" type="text" ref={register({ required: true, pattern: /[A-Za-z ñ]+/ })} />
                        <div className="text-danger text-small d-block mb-2">{errors.name && t("gral_data.msjname")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="position" placeholder={t("gral_data.position")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="company" placeholder={t("gral_data.company")} className="form-control" type="text" ref={register({ required: true })} />
                        <div className="text-danger text-small d-block mb-2">{errors.company && t("gral_data.msjcompany")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="phone" placeholder={t("gral_data.phone")} className="form-control" type="tel" ref={register({
                            required: true, maxLength: 10, minLength: 7, pattern: /^[0-9]+/
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.phone && t("gral_data.msjphone")}</div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="email" placeholder={t("gral_data.email")} className="form-control" type="text" ref={register({
                            required: true,
                            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.email && t("gral_data.msjemail")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="sector" placeholder={t("gral_data.sector")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="project" placeholder={t("gral_data.project")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        <input name="adress" placeholder={t("gral_data.adress")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <div >
                            <h4>{t("gral_data.application")}</h4>
                            <br />
                            <select id="applicationHum" className="form-control" name="application" ref={register} onChange={Other}>
                                <option className="form-control" value="">{t("gral_data.default")}</option>
                                <option className="form-control" value="Farmacéutica">{t("gral_data.pharma")}</option>
                                <option className="form-control" value="Hospitalaria">{t("gral_data.hosp")}</option>
                                <option className="form-control" value="HVAC">{t("gral_data.hvac")}</option>
                                <option className="form-control" value="otro">{t("gral_data.other")}</option>
                            </select>
                        </div>
                        <br />
                        <input id="idOtherHum" name="otherApplication" style={{ display: "none" }} placeholder={t("gral_data.specify")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <br />
                <h4>{t("formhum.kind")}</h4>
                <div className="row">
                    <div className="col-sm">
                        <label>{t("formhum.electric")}</label>
                        <select className="form-control" name="typeElectric" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="220 VCA - 2F">{t("220 VCA - 2F")}</option>
                            <option className="form-control" value="220 VCA -3F">{t("220 VCA -3F")}</option>
                            <option className="form-control" value="440 VCA - 2F">{t("440 VCA - 2F")}</option>
                            <option className="form-control" value="440 VCA - 3F">{t("440 VCA - 3F")}</option>
                        </select>
                    </div>
                    <div className="col-sm">
                        <label>{t("formhum.gas")}</label>
                        <select className="form-control" name="typeGas" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="Gas natural">{t("formhum.gas1")}</option>
                            <option className="form-control" value="Gas LP">{t("formhum.gas2")}</option>
                        </select>
                    </div>
                    <div className="col-sm">
                        <label>{t("formhum.steam")}</label>
                        <select className="form-control" name="typeVapor" ref={register}>
                            <option className="form-control" >{t("Seleccione uno")}</option>
                            <option className="form-control" value="Vapor directo">{t("formhum.dirstea")}</option>
                            <option className="form-control" value="Vapor - vapor">{t("formhum.steste")}</option>
                        </select>
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-sm" >{t("formhum.adiabatic")}
                        <input className="form-control" type="checkbox" name="typeadiabatic" value="Adiabatico" ref={register}></input>
                    </div>
                    <div className="col-sm">{t("gral_data.other")}
                        <input className="form-control" type="text" name="othertype" placeholder={t("gral_data.specify")} ref={register}></input>
                    </div>
                </div>
                <br />
                <h4>{t("formhum.where")}</h4>
                <div className="row">
                    <div className="col-sm">
                        <label >{t("formhum.duct")}</label>
                        <br />
                        <label >{t("formhum.have_dimen")}</label>
                        <select className="form-control" name="dimensionDuct" id="dimensionDuct" onChange={hideDuct} ref={register}>
                            <option className="form-control" value="">{t("gral_data.default")}</option>
                            <option className="form-control" value="Si">{t("formhum.y")}</option>
                            <option className="form-control" value="No">{t("formhum.n")}</option>
                        </select>
                        <br />
                        <input id="idDuct" name="measureDuct" style={{ display: "none" }} placeholder={t("formhum.dimen")} className="form-control" type="text" ref={register} />
                        <br />
                    </div>
                    <div className="col-sm">
                        <label >{t("Uma")}</label>
                        <br />
                        <label >{t("formhum.have_dimen")}</label>
                        <select className="form-control" name="dimensionUma" id="dimensionUma" onChange={hideUma} ref={register}>
                            <option className="form-control" value="">{t("gral_data.default")}</option>
                            <option className="form-control" value="Si">{t("formhum.y")}</option>
                            <option className="form-control" value="No">{t("formhum.n")}</option>
                        </select>
                        <br />
                        <input id="idUma" name="measureUma" style={{ display: "none" }} placeholder={t("formhum.dimen")} className="form-control" type="text" ref={register} />
                        <br />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-sm">
                        <h4>{t("formhum.water")}</h4>
                        <select className="form-control" name="typeWater" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="Del grifo">{t("formhum.tap")}</option>
                            <option className="form-control" value="Potable">{t("formhum.potab")}</option>
                            <option className="form-control" value="Suavizada">{t("formhum.smoot")}</option>
                            <option className="form-control" value="Desionizada">{t("formhum.deio")}</option>
                            <option className="form-control" value="Osmosis inversa">{t("formhum.osmo")}</option>
                        </select>
                    </div>
                </div>
                <br />
                <div className="row">

                    <div className="col-sm">
                        <h4>{t("formhum.city")}</h4>
                        <input name="city" placeholder={t("formhum.city")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="col-sm">
                        <h4>{t("formhum.condi")}</h4>
                        <input name="outdoorConditions" placeholder={t("°C , %RH")} className="form-control" type="text" ref={register} />
                    </div>

                    <div className="col-sm">
                        <h4>{t("formhum.reach")}</h4>
                        <input name="conditionsReach" placeholder={t("°C , %RH")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <br />
                <div className="row">
                    <div className="col-sm">
                        <h4>{t("formhum.auto")}</h4>
                        <select className="form-control" name="needAutomat" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="Yes">{t("formhum.y")}</option>
                            <option className="form-control" value="No">{t("formhum.n")}</option>
                        </select>
                    </div>
                    <div className="col-sm">
                        <h4>{t("formhum.control")}</h4>
                        <select className="form-control" name="needComunication" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="BACNET">{t("BACNET")}</option>
                            <option className="form-control" value="MODBUS">{t("MODBUS")}</option>
                            <option className="form-control" value="NO">{t("formhum.n")}</option>
                        </select>
                    </div>
                </div>
                <br />
                <button className="btn-send">{t("gral_data.send")}</button>
            </form>
        </>
    )
}

export default FormHum;