import React from 'react';

const SocialBar = () => (
    <div className='social-bar-header'>
        <a href="tel:+5215576311927" datatype="phone">
            <i className="fa fa-phone fa-rotate-90 fa-2x"/>
        </a>

        <a target='_blank' href="mailto:hello@gyeproyectos.com" datatype="mail">
            <i class="fa fa-envelope fa-2x"/>
        </a>

        <a target='_blank' href="https://www.linkedin.com/company/gye-proyectos-mexico/about/">
            <i className="fa fa-linkedin fa-2x"/>
        </a>

        <a target='_blank' href="https://wa.me/5215576311927?text=Hola%20GYE%20quiero%20informes">
            <i className="fa fa-whatsapp fa-2x"/>
        </a>
    </div>
);

export default SocialBar;