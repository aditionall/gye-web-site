import React from 'react';
import FadeInSection from '../components/FadeInSection'

const MarketItemRight = (props) => (
    <div className="market-item row">
        
        <div className="text-right text-expand col-xs-12 col-md-6 container">
            <FadeInSection direction="to-right" startVisible={true} >
                <div className="row">
                    <div className="title col-12 d-flex justify-content-between ">
                        <img className="image-small" src={props.icon}/>
                        <h2>{props.title}</h2>
                    </div>
                    
                    <p className="col-12 align-middle text-right">
                        {props.description}
                    </p>
                </div>
            </FadeInSection>
        </div>

        <div className="image-expand col-xs-12 col-md-6">
            <FadeInSection direction="to-left" startVisible={true} >
                <img src={props.img} alt=""/>
            </FadeInSection>
            
        </div>
    </div>
  
);

export default MarketItemRight;
