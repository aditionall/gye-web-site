import React, { useState } from "react";
import Axios from "axios";
import clearskyd from "../assets/static/icon-weather/01d.png"
import clearskyn from "../assets/static/icon-weather/01n.png"
import fewcloudsd from "../assets/static/icon-weather/02d.png"
import fewcloudsn from "../assets/static/icon-weather/02n.png"
import scatteredclouds from "../assets/static/icon-weather/03d.png"
import brokenclouds from "../assets/static/icon-weather/04d.png"
import showerrain from "../assets/static/icon-weather/09d.png"
import raind from "../assets/static/icon-weather/10d.png"
import rainn from "../assets/static/icon-weather/10n.png"
import thunderstorm from "../assets/static/icon-weather/11d.png"
import snow from "../assets/static/icon-weather/13d.png"
import mist from "../assets/static/icon-weather/50d.png"

function WeatherInfo() {

    const [city, setCity] = useState("Cdmx");
    const [temp, setTemp] = useState();
    const [icon, setIcon] = useState();
    const [lat, setLat] = useState();
    const [long, setLong] = useState();

    // const [lat, setLat] = useState();
    // const [long, setLong] = useState();
    const [flag, setFlag] = useState(false);

    if (navigator.geolocation) {
        var success = function (position) {
            var latitud = position.coords.latitude.toString()
            var longitud = position.coords.longitude.toString()
            setLat(latitud)
            setLong(longitud)
        }
        navigator.geolocation.getCurrentPosition(success, function (msg) {
            // console.error("No hay permisos");
            setLat("19.433261912491297")
            setLong("-99.13100093070715")
        });
    }

    // if ("geolocation" in navigator) {
    //     navigator.geolocation.getCurrentPosition(getPosition);
    // } else {
    //     setLat("19.433261912491297")
    //     setLong("-99.13100093070715")
    // }

    // function getPosition(position) {
    //     let lat = position.coords.latitude.toString()
    //     let long = position.coords.longitude.toString()
    //     setLat(lat)
    //     setLong(long)
    // }

    function getTemp() {
        Axios({
            url: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=a34fd63df7b058a02b3c2f9ff45d7fcc&units=metric`,
        })
            .then((response) => { setCity(response.data.name); setTemp(response.data.main.temp); setIcon(response.data.weather[0].icon); })
            .catch((error) => { console.log(error); });
    }

    if (lat !== undefined && long !== undefined) {
        if (flag === false) {
            getTemp()
            // console.log("API EJECUTADA");
            setFlag(true);
        }
    }


    switch (icon) {
        case "01d":
            setIcon(clearskyd);
            break;
        case "01n":
            setIcon(clearskyn);
            break;
        case "02d":
            setIcon(fewcloudsd);
            break;
        case "02n":
            setIcon(fewcloudsn);
            break;
        case "03d":
            setIcon(scatteredclouds);
            break;
        case "03n":
            setIcon(scatteredclouds);
            break;
        case "04d":
            setIcon(brokenclouds);
            break;
        case "04n":
            setIcon(brokenclouds);
            break;
        case "09d":
            setIcon(showerrain);
            break;
        case "09n":
            setIcon(showerrain);
            break;
        case "10d":
            setIcon(raind);
            break;
        case "10n":
            setIcon(rainn);
            break;
        case "11d":
            setIcon(thunderstorm);
            break;
        case "13d":
            setIcon(snow);
            break;
        case "13n":
            setIcon(snow);
            break;
        case "50d":
            setIcon(mist);
            break;
        case "50n":
            setIcon(mist);
            break;

    }

    return (
        <div >
            <div className="weather">
                <h1>{temp}° C</h1>
                <div className="weather-content">
                    <label>{city}</label>
                    <img className="weather-img" src={icon} alt="" />
                </div>
            </div>
        </div>
    );
}

export default WeatherInfo;
