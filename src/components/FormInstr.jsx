import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import { useForm, Controller } from 'react-hook-form';
import { db } from '../firebase/firebase-config';
import Swal from 'sweetalert2'
import logo from "../assets/static/rotronic-logo.svg";

function FormInstr() {
    var hoy = new Date();
    var fecha = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear();
    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();

    const [t, i18n] = useTranslation("global")
    const { register, errors, handleSubmit } = useForm();
    const [Entradas, setEntradas] = useState('')

    const InsertFormInstr = (linkObject) => {
        db.collection('formInstr').doc().set(linkObject)
        console.log('OK formInstr' + fecha + " " + hora);
        Enviado();
    }

    const onSubmit = (data, e) => {
        setEntradas([...Entradas, data])
        InsertFormInstr(data);
        e.target.reset();
    };

    const Other = () => {
        var idOther = document.getElementById("idOtherIns");
        var other = document.getElementById("applicationIns").value;
        if (other === "otro") {
            idOther.style.display = "block";
        } else {
            idOther.style.display = "none";
        }
    }

    const Enviado = () => {
        Swal.fire({
            title: t("alert.tanks"),
            text: t("alert.msjcontact"),
            icon: "success"
        }
        )
    }

    return (
        <div>
            <div className="brand-container">
                <img src={logo} className="" />
            </div>
            <form className="container-form" onSubmit={handleSubmit(onSubmit)}>
                <input name="date" type="hidden" value={fecha + " " + hora} ref={register} />
                <input name="form" type="hidden" value="Instrumentación Rotronic" ref={register} />
                <input name="idform" type="hidden" value="formInstr" ref={register} />
                <input name="status" type="hidden" value="" ref={register} />
                <input name="assigned" type="hidden" value="" ref={register} />
                <input name="sellercomments" type="hidden" value="" ref={register} />
                <input name="admincomments" type="hidden" value="" ref={register} />
                <h4>{t("gral_data.title")}</h4>
                <br />
                <div className="row" >
                    <div className="col-sm">
                        <input name="name" placeholder={t("gral_data.name")} className="form-control" type="text" ref={register({ required: true, pattern: /[A-Za-z ñ]+/ })} />
                        <div className="text-danger text-small d-block mb-2">{errors.name && t("gral_data.msjname")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="position" placeholder={t("gral_data.position")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="company" placeholder={t("gral_data.company")} className="form-control" type="text" ref={register({ required: true })} />
                        <div className="text-danger text-small d-block mb-2">{errors.company && t("gral_data.msjcompany")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="phone" placeholder={t("gral_data.phone")} className="form-control" type="tel" ref={register({
                            required: true, maxLength: 22, minLength: 7, pattern: /^[0-9]+/
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.phone && t("gral_data.msjphone")}</div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="email" placeholder={t("gral_data.email")} className="form-control" type="text" ref={register({
                            required: true,
                            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        })} />
                        <div className="text-danger text-small d-block mb-2">{errors.email && t("gral_data.msjemail")}</div>
                    </div>
                    <div className="col-sm">
                        <input name="sector" placeholder={t("gral_data.sector")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <input name="project" placeholder={t("gral_data.project")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="form-group col-md">
                        <input name="adress" placeholder={t("gral_data.adress")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <h4>{t("gral_data.application")}</h4>
                        <br />
                        <div >
                            <select id="applicationIns" className="form-control" name="application" ref={register} onChange={Other}>
                                <option className="form-control" value="">{t("gral_data.default")}</option>
                                <option className="form-control" value="Farmacéutica">{t("gral_data.pharma")}</option>
                                <option className="form-control" value="Hospitalaria">{t("gral_data.hosp")}</option>
                                <option className="form-control" value="HVAC">{t("gral_data.hvac")}</option>
                                <option className="form-control" value="otro">{t("gral_data.other")}</option>
                            </select>
                        </div>
                        <br />
                        <input id="idOtherIns" name="otherApplication" style={{ display: "none" }} placeholder={t("gral_data.specify")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <br />
                <h4>{t("forminstr.measu")}</h4>
                <br />
                <div className="row">
                    <div className="form-group col-md" >
                        <label htmlFor="humedad">{t("forminstr.humi")}</label>
                        <input id="humedad" className="form-control" type="checkbox" name="measureHumidity" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="temp">{t("forminstr.temp")}</label>
                        <input id="temp" className="form-control" type="checkbox" name="measureTemperature" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="co2">{t("forminstr.co2")}</label>
                        <input id="co2" className="form-control" type="checkbox" name="measureCo2" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="pressuredif">{t("forminstr.pressuredif")}</label>
                        <input id="pressuredif" className="form-control" type="checkbox" name="measureDifPressure" value="yes" ref={register}></input>
                    </div>
                </div>
                <div className="row">
                    <div className="form-group col-md" >
                        <label htmlFor="pressure">{t("forminstr.pressure")}</label>
                        <input id="pressure" className="form-control" type="checkbox" name="measurePresion" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="water">{t("forminstr.water")}</label>
                        <input id="water" className="form-control" type="checkbox" name="measurewater" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="dew">{t("forminstr.dew")}</label>
                        <input id="dew" className="form-control" type="checkbox" name="measuredew" value="yes" ref={register}></input>
                    </div>
                    <div className="form-group col-md">
                        <label htmlFor="O2">{t("O2")}</label>
                        <input id="O2" className="form-control" type="checkbox" name="measureO2" value="yes" ref={register}></input>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <h4>{t("forminstr.instrum")}</h4>
                        <br />
                        <select className="form-control" name="instrument" ref={register}>
                            <option className="form-control" >{t("gral_data.default")}</option>
                            <option className="form-control" value="Sondas">{t("forminstr.probes")}</option>
                            <option className="form-control" value="Transmisores">{t("forminstr.transm")}</option>
                            <option className="form-control" value="Paneles de cuarto limpio">{t("forminstr.panel")}</option>
                            <option className="form-control" value="Data Loggers">{t("forminstr.logger")}</option>
                            <option className="form-control" value="Instrumentos de mano">{t("forminstr.hand")}</option>
                            <option className="form-control" value="Equipos de calibración">{t("forminstr.calibrat")}</option>
                            <option className="form-control" value="Analizadores">{t("forminstr.analyz")}</option>
                        </select>
                    </div>
                </div>
                <br />
                <div className="row" >
                    <div className="col-sm">
                        <h4>{t("forminstr.range")}</h4>
                        <br />
                        <input name="rangesToMeasure" placeholder={t("gral_data.wich")} className="form-control" type="text" ref={register} />
                    </div>
                    <div className="col-sm">
                        <h4 >{t("forminstr.mini")}</h4>
                        <br />
                        <input name="requireMinPress" placeholder={t("gral_data.wich")} className="form-control" type="text" ref={register} />
                    </div>
                </div>
                <br />
                <div className="col-sm">
                    <h4>{t("forminstr.monitor")}</h4>
                    <br />
                    <textarea
                        name="needMonitorSystems"
                        rows="5"
                        placeholder={t("forminstr.descri")}
                        className="form-control textarea"
                        type="text"
                        ref={register}
                    ></textarea>
                </div>
                <br />
                <button className="btn-send">{t("gral_data.send")}</button>
            </form>
        </div >
    )
}

export default FormInstr;