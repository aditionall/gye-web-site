import React from "react";
import logo from "../assets/static/logo-light.png";
import DevomAdvertising from "./DevomAdvertising";
import { useTranslation } from "react-i18next";

//class  extends React.Component{

      
    function Footer() {
        const [t, i18n] = useTranslation("global");
        return (
            <footer className='footerNormal row justify-content-center text-center pt-5'>
                
                <div className='col-lg-4 col-md-6 col-sm-12 d-flex flex-column '>
                    <div className='p-2'>
                        {t("footer.contact")}
                    </div>

                    <div className='p-2'>
                        <a href="tel:+5215512704712" datatype="phone">
                        <i className="fa fa-phone fa-rotate-90 fa-lg px-2"/>+5215512704712 </a>
                    </div>

                    <div className='p-2'>
                        <a href="tel:+5215576311927" datatype="phone">
                        <i className="fa fa-phone fa-rotate-90 fa-lg px-2"/>+5215576311927 </a>
                    </div>

                    <div className='p-2'>
                        <a href="tel:+5215525891532" datatype="phone">
                        <i className="fa fa-phone fa-rotate-90 fa-lg px-2"/>+5215525891532 </a>
                    </div>

                    <div className='p-2'>
                        <a target='_blank' href="mailto:hello@gyeproyectos.com" datatype="mail">
                        <i className="fa fa-envelope fa-lg px-2"/>hello@gyeproyectos.com</a>
                    </div>
                </div>
                <hr/>
                <div className='col-lg-3 col-md-5 col-sm-12 my-auto'>
                    <div className='p-2'>
                        <img className='align-middle' src={logo} width='50px'/>
                    </div>
              
                    <div className='p-1'>
                        <a target='_blank' href="https://www.linkedin.com/company/gye-proyectos-mexico/">
                            <i className="fa fa-linkedin fa-2x"/></a>
                        <a target='_blank' href="https://wa.me/5215576311927?text=Hola%20GYE%20quiero%20informes">
                            <i className="fa fa-whatsapp fa-2x"/></a>
                    </div>
                </div>
                <hr/>
                <div className='col-lg-4 col-md-6 col-sm-12 d-flex flex-column'>
                    <div className='p-2'>
                        {t("footer.terms")}
                    </div>
                    <div className='p-2'>
                        <a href="/">{t("footer.privacy")}</a>
                    </div>
                    <div className='p-2'>
                        <a href="/">{t("footer.terms2")}</a>
                    </div>
                
                </div>
                <div className='col-12 '>
                    <p className='p-2 copy'> 
                        2020 &copy; GYE Proyectos Mexico All right reserved
                    </p>
                </div>
                <DevomAdvertising/>
            </footer>
        );
      }

//}

export default Footer;