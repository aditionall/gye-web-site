import React from 'react';
import FadeInSection from '../components/FadeInSection'

const MarketItemLeft = (props) => (
    <div className="market-item row flex-column-reverse flex-md-row ">

        <div className="image-expand col-xs-12 col-md-6">
            <FadeInSection direction="to-right" startVisible={true} >
                <img src={props.img} alt=""/>
            </FadeInSection>
        </div>

        <div className="text-left text-expand col-xs-12 col-md-6 container">
            <FadeInSection direction="to-left" startVisible={true} >
                <div className="row">
                    <div className="title col-12 d-flex justify-content-between">
                        <h2>{props.title}</h2>   
                        <img className="image-small" src={props.icon}/>
                    </div>
                    
                    <p className="col-12">
                        {props.description}
                    </p>
                </div>
            </FadeInSection>
        </div>

    </div>
);

export default MarketItemLeft;