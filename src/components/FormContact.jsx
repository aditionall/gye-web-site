import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import { useForm } from 'react-hook-form';
import { db } from '../firebase/firebase-config';
import Swal from 'sweetalert2'

function FormContact() {
    var hoy = new Date();
    var fecha = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear();
    var hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();


    const [t, i18n] = useTranslation("global")
    const { register, errors, handleSubmit } = useForm();
    const [Entradas, setEntradas] = useState('')

    const insertFormCont = (linkObject) => {
        db.collection('formContact').doc().set(linkObject)
        console.log('OK formContact ' + fecha + " " + hora);
        Enviado();
    }

    const onSubmit = (data, e) => {
        setEntradas([...Entradas, data])
        insertFormCont(data);
        e.target.reset();
    };

    const Enviado = () => {
        Swal.fire({
            title: t("alert.tanks"),
            text: t("alert.msjcontact"),
            icon: "success"
        }
        )
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="container-form" >
            <h4>{t("contact.leav")}</h4>
            <br />
            <input name="date" type="hidden" value={fecha + " " + hora} ref={register} />
            <input name="form" type="hidden" value="Contacto" ref={register} />
            <input name="idform" type="hidden" value="formContact" ref={register} />
            <input name="status" type="hidden" value="" ref={register} />
            <input name="assigned" type="hidden" value="" ref={register} />
            <input name="sellercomments" type="hidden" value="" ref={register} />
            <input name="admincomments" type="hidden" value="" ref={register} />
            <div className="row" >
                <div className="col-sm">
                    <input name="name" placeholder={t("gral_data.name")} className="form-control" type="text"
                        ref={register({ required: true, pattern: /[A-Za-z ñ]+/ })}
                    ></input>
                    <div className="text-danger text-small d-block mb-2">{errors.name && t("gral_data.msjname")}</div>
                </div>
            </div>
            <div className="row">
                <div className="col-sm">
                    <input
                        name="email"
                        placeholder={t("gral_data.email")}
                        className="form-control"
                        type="email"
                        ref={register({
                            required: true,
                            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        })}
                    ></input>
                    <div className="text-danger text-small d-block mb-2">{errors.email && t("gral_data.msjemail")}</div>
                </div>
                <div className="col-sm">
                    <input
                        name="phone"
                        placeholder={t("gral_data.phone")}
                        className="form-control"
                        type="tel"
                        ref={register({
                            required: true, maxLength: 10, minLength: 7,
                            pattern: /^[0-9]+/
                        })}
                    ></input>
                    <div className="text-danger text-small d-block mb-2">{errors.phone && t("gral_data.msjphone")}</div>
                </div>
            </div>
            <div className="row" >
                <div className="col-sm" >
                    <textarea
                        id="comment"
                        name="comment"
                        rows="4"
                        placeholder={t("gral_data.comm")}
                        className="form-control textarea"
                        type="text"
                        ref={register({
                            maxLength: 250
                        })}
                    ></textarea>
                    <div className="text-danger text-small d-block mb-2">{errors.comment && t("gral_data.msjcomm")}</div>
                </div>
            </div>
            <br />
            <button className="btn-send">{t("gral_data.send")}</button>
        </form>
    )
}

export default FormContact;