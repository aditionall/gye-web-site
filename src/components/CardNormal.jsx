import React,{Component} from "react";

const CardNormal = (props) => (
    <div className ="card-info">
        <hr />
        <h2>{props.title}</h2>
        <p>
            {props.text}
        </p>
    </div>
)

export default CardNormal;