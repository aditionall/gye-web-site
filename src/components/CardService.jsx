import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { useTranslation } from "react-i18next";

function CardService(props) {
    const [t, i18n] = useTranslation("global")

    const linkToWa = t("landing.message_wa") + t("landing.message_wa_over") + props.title

    window.addEventListener('scroll', function () {
        /*let animacion = document.getElementById("cardServices");
        let posicionObj1 = animacion.getBoundingClientRect().top;
        console.log(posicionObj1);
        //console.log(animacion);
        let tamañoPantalla = window.innerHeight;
        //console.log(tamañoPantalla);
        if(posicionObj1 < 132){
            animacion.className +="face1";
        }*/
    })

    return (
        <div id="cardServices" className="card-service col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div className="face face1">
                <div className="content">
                    <p>
                        {props.text}
                    </p>
                    <div className="row mx-auto">
                        <Link to="/Contact" className="col-12">
                            <span></span>
                            {t("landing.contact_us")}
                        </Link>

                        <a target='_blank' className="col-12" href={linkToWa}>

                            {t("landing.send_wa")}
                            <i className="fa fa-whatsapp fa-2x px-2" />

                        </a>

                    </div>
                </div>

            </div>

            <div id="face2" className="face face2">
                <h2>{props.title}</h2>
            </div>

        </div>

    )
}



export default CardService;