import React from 'react'

function SearchPost() {
    return (
        <div >
            <ol style={{ display: "flex", justifyContent: "space-evenly", listStyle: "none" }}>
                <li>Todas las entradas</li>
                <li>HVAC y el COVID-19</li>
                <li>Paneles Solares</li>
                <li>
                    <input type="search" name="search" placeholder="..." />
                    <input type="submit" value="Buscar" />
                </li>
            </ol>
        </div >
    )
}

export default SearchPost
