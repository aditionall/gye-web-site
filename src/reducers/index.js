const LANGES = 'LANGUAGE/SPANISH'
const LANGEN = 'LANGUAGE/ENGLISH'

const langEs = () => ({
    type: LANGES,
})

const langEn = () => ({
    type: LANGEN,
})

const initialState = 0

export default function(state = initialState, action){
    switch (action.type){
        case LANGES:
            return 0
        case LANGEN:
            return 1

    }

}