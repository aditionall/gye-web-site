import React from 'react'
import Footer from '../components/Footer';
import Header from '../components/Header';
import Comments from '../components/Comments';
import Recent from '../components/Recent';
import SearchPost from '../components/SearchPost';
import Post from './Post';

function Blog() {
    return (
        <div style={{ background: "white"}}>
            <Header />
            <SearchPost />
            <Post />
            <Comments />
            <Recent />
            <Footer />
        </div>
    )
}

export default Blog
