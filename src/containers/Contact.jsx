import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import Header from '../components/Header';
import Footer from './../components/FooterParallax';
import FormContact from '../components/FormContact';
import WeatherInfo from './../components/WeatherInfo';


function Contact() {

    const [t, i18n] = useTranslation("global")

    return (
        <>
        
            <Header />
            <div className="formgye" >
                <div className="contanier">
                    <hr />
                    <h1>{t("contact.title")}</h1>
                    <div className="card-form col-12 col-md-8 col-lg-8 col-xl-8 form-contact">
                        <FormContact />
                    </div>
                </div>
                

            </div>
            <Footer />
        </>
    )
}

export default Contact;