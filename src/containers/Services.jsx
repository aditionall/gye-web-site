import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import Header from '../components/Header';
import Footer from '../components/FooterParallax';
import CardService from '../components/CardService';
import background from "../assets/static/decoration-top-general.png";
import data from '../translations/es/global.json';

function Services() {

    const [t, i18n] = useTranslation("global")

    const handleClickSelected = (typeFormToShow) => {
        //console.log(linkObject);
        var staticcols = " col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 "
        var servhvac = document.getElementById("servhvac");
        var servwater = document.getElementById("servwater");

        var btIdHvac = document.getElementById("btIdHvac");
        var btIdwater = document.getElementById("btIdwater");

        switch (typeFormToShow) {
            case 1:
                servhvac.style.display = "flex";
                servwater.style.display = "none";
                btIdHvac.className = "btn-selected" + staticcols;
                btIdwater.className = "btn-form" + staticcols;;

                break
            case 2:
                servhvac.style.display = "none";
                servwater.style.display = "flex";
                btIdHvac.className = "btn-form" + staticcols;
                btIdwater.className = "btn-selected" + staticcols;;

                break

        }
    }

    return (
        <>
            <Header />
            <div className="services" >
                <img className='decoration-top-general' src="" alt="" />

                <div className="contanier-services">
                    <hr />
                    <h1>{t("services.title")}</h1>

                    <div className="row col-8 mx-auto">
                        <button id="btIdHvac" className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 btn-selected " onClick={() => handleClickSelected(1)}>{t("services.section1")}</button>
                        <button id="btIdwater" className="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 btn-form " onClick={() => handleClickSelected(2)}>{t("services.section2")}</button>
                    </div>

                    <div id="servhvac" style={{ display: "flex" }} className="services-list row ">
                        <CardService title={t("services.subtitle1")} text={t("services.txt1")} />
                        <CardService title={t("services.subtitle2")} text={t("services.txt2")} />
                        <CardService title={t("services.subtitle3")} text={t("services.txt3")} />
                        <CardService title={t("services.subtitle4")} text={t("services.txt4")} />
                        <CardService title={t("services.subtitle5")} text={t("services.txt5")} />
                        <CardService title={t("services.subtitle6")} text={t("services.txt6")} />
                        <CardService title={t("services.subtitle7")} text={t("services.txt7")} />
                        <CardService title={t("services.subtitle8")} text={t("services.txt8")} />
                    </div>
                    <div id="servwater" style={{ display: "none" }} className="services-list row">
                        <CardService title={t("services.subtitle9")} text={t("services.txt9")} />
                        <CardService title={t("services.subtitle10")} text={t("services.txt10")} />
                        <CardService title={t("services.subtitle11")} text={t("services.txt11")} />
                        <CardService title={t("services.subtitle12")} text={t("services.txt12")} />
                        <CardService title={t("services.subtitle13")} text={t("services.txt13")} />
                        <CardService title={t("services.subtitle14")} text={t("services.txt14")} />
                        <CardService title={t("services.subtitle15")} text={t("services.txt15")} />
                        <CardService title={t("services.subtitle16")} text={t("services.txt16")} />
                        <CardService title={t("services.subtitle17")} text={t("services.txt17")} />
                    </div>

                </div>
            </div>
            <Footer />
        </>
    )
}

export default Services;