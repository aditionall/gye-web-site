import React, { Component } from 'react';
import Header from '../components/Header';
import FooterParallax from '../components/FooterParallax';
import CardNormal from '../components/CardNormal';
import MarketingItemLeft from '../components/MarketItemLeft';
import MarketingItemRight from '../components/MarketItemRight';
import FadeInSection from '../components/FadeInSection'
import mkt1 from "../assets/static/market1.png";
import mkt2 from "../assets/static/market2.png";
import mkt3 from "../assets/static/market3.png";
import icon1 from "../assets/static/icon-market1.png";
import icon2 from "../assets/static/icon-market2.png";
import icon3 from "../assets/static/icon-market3.png";
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';
import WeatherInfo from './../components/WeatherInfo';

function LandingPage() {

    const [t, i18n] = useTranslation("global")

    return (
        <div className='container-landing'>

            <div className="below-hero" >

                <div className="we-do" >
                    <img className='decoration-top' src='' alt="" />

                    <FadeInSection direction="to-up" startVisible={false} >
                        <div className="card-list row ">
                            <div className="col-lg-4 col-md-12">
                                <CardNormal title={t("landing.what")} text={t("landing.what_ans")} />
                            </div>
                            <div className="col-lg-4 col-md-12">
                                <CardNormal title={t("landing.how")} text={t("landing.how_ans")} />
                            </div>
                            <div className="col-lg-4 col-md-12">
                                <CardNormal title={t("landing.give")} text={t("landing.give_ans")} />
                            </div>

                        </div>
                    </FadeInSection>

                    <div className="title">
                        <FadeInSection direction="to-up" startVisible={false}>
                            <hr />
                            <h1>{t("landing.us")}</h1>
                        </FadeInSection>
                    </div>

                </div>

                <div className="us-info" >
                    <div className="description">
                        <FadeInSection direction="to-up" startVisible={false}>
                            <p className="" >{t("landing.us_info")}</p>
                        </FadeInSection>
                    </div>
                    <img className='decoration-bottom' src='' alt="" />
                    <div className="title">
                        <FadeInSection direction="to-up" startVisible={false} >
                            <hr />
                            <h1>{t("landing.market")}</h1>
                        </FadeInSection>

                    </div>
                </div>


                <div className="our-market" >
                    <MarketingItemRight title={t("landing.health")} img={mkt1} icon={icon1} description={t("landing.health_desc")} />
                    <MarketingItemLeft title={t("landing.pharma")} img={mkt2} icon={icon2} description={t("landing.pharma_desc")} />
                    <MarketingItemRight title={t("landing.energy")} img={mkt3} icon={icon3} description={t("landing.energy_desc")} />
                </div>

                <FooterParallax />

            </div>

            <div className="hero">
                <Header bg="transparent" />
                <hr />

                <div className='impact-text'>
                    <h1>{t("landing.impact1")}<br />{t("landing.impact2")}</h1>
                    <h3>{t("landing.impact3")}</h3>
                    <h3>{t("landing.impact4")}</h3>
                </div>
                <div className='social-bar'>

                    <a href="tel:+5215576311927" datatype="phone">
                        <i className="fa fa-phone fa-rotate-90 fa-2x" />
                    </a>

                    <a target='_blank' href="mailto:hello@gyeproyectos.com" datatype="mail">
                        <i className="fa fa-envelope fa-2x" />
                    </a>

                    <a target='_blank' href="https://www.linkedin.com/company/gye-proyectos-mexico/">
                        <i className="fa fa-linkedin fa-2x" />
                    </a>

                    <a target='_blank' href={t("landing.message_wa")}>
                        <i className="fa fa-whatsapp fa-2x" />
                    </a>




                </div>

                <div className='btn-go' >
                    <Link to="/Contact">
                        <span></span>
                        {t("landing.contact_us")}
                    </Link>
                </div>

                <WeatherInfo />

            </div>

        </div>
    )
}


export default LandingPage;