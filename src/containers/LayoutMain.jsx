import React from 'react' ;
import Header from '../components/Header';
import Footer from '../components/Footer';

function LayoutMain(props){
    return (
        <React.Fragment>
            {props.children} 
        </React.Fragment>
    );
}

export default LayoutMain;