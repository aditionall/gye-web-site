import React, { Component } from 'react';
import {
  Router,
  HashRouter,
  Switch,
  Route,
  Redirect,
  BrowserRouter
} from 'react-router-dom';
import LayoutMain from './LayoutMain'
import LandingPage from './LandingPage';
import NotFound from './NotFound';
import { connect } from 'react-redux';
import Quote from './Quote';
import Services from './Services'
import Contact from './Contact';
import FormHum from './../components/FormHum';
import FormInstr from './../components/FormInstr';
import FormBMS from './../components/FormBMS';
import Blog from './Blog';

class App extends Component {
  render() {
    console.log(this.props)

    return (
      <BrowserRouter>
        <LayoutMain>
          <Switch>
            <Route exact path="/Blog" component={Blog} />
            <Route exact path="/FormHum" component={FormHum} />
            <Route exact path="/FormInstr" component={FormInstr} />
            <Route exact path="/FormBMS" component={FormBMS} />
            <Route exact path="/Services" component={Services} />
            <Route exact path="/Quote" component={Quote} />
            <Route exact path="/Contact" component={Contact} />
            <Route exact path="/" component={LandingPage} />
          </Switch>
        </LayoutMain>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = state => {
  console.log({ state })
  return {
    lang: state,
  }
}

const mapDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(App);