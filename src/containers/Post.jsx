import React from 'react'

function Post() {
    return (
        <div style={{ textAlign: "center" }}>
            <h1>Tiulo del articulo</h1>
            <br />
            <img src="" alt="Imagen del articulo" />
            <br />
            <p>Contenido del articulo</p>
        </div>
    )
}

export default Post
