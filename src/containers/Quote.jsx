import React, { useState } from 'react';
import { useTranslation } from "react-i18next";
import Header from '../components/Header';
import Footer from '../components/FooterParallax';
import FormInstr from './../components/FormInstr';
import FormBMS from '../components/FormBMS';
import FormHum from './../components/FormHum';
//import { render } from '@testing-library/react';

const Quote = () => {

    const [t, i18n] = useTranslation("global")
    //const [show, setShow] = useState(true);

    function handleClickSelected(typeFormToShow) {
        var btIdInstrum = document.getElementById("btIdInstrum");
        var btIdHum = document.getElementById("btIdHum");
        var btIdBms = document.getElementById("btIdBms");

        var formInstrum = document.getElementById("formIdInstrum");
        var formHum = document.getElementById("formIdHum");
        var formBms = document.getElementById("formIdBms");

        var staticcols = " col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 "

        switch (typeFormToShow) {
            case 1:
                formInstrum.style.display = "block";
                formHum.style.display = "none";
                formBms.style.display = "none";
                btIdInstrum.className = "btn-selected" + staticcols;
                btIdHum.className = "btn-form" + staticcols;
                btIdBms.className = "btn-form" + staticcols;
                break
            case 2:
                formInstrum.style.display = "none";
                formHum.style.display = "block";
                formBms.style.display = "none";
                btIdInstrum.className = "btn-form" + staticcols;
                btIdHum.className = "btn-selected" + staticcols;
                btIdBms.className = "btn-form" + staticcols;

                break
            case 3:
                formInstrum.style.display = "none";
                formHum.style.display = "none";
                formBms.style.display = "block";
                btIdInstrum.className = "btn-form" + staticcols;
                btIdHum.className = "btn-form" + staticcols;
                btIdBms.className = "btn-selected" + staticcols;
                break
        }
    }

    return (
        <>
            <Header />
            <div className="formgye" >
                <div className="contanier">
                    <hr />
                    <h1>{t("quote.title")}</h1>

                    <div className="row">
                        <button id="btIdInstrum" className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 btn-selected" onClick={() => handleClickSelected(1)}>{t("forminstr.btninst")}</button>
                        <button id="btIdHum" className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 btn-form " onClick={() => handleClickSelected(2)}>{t("formhum.btnhum")}</button>
                        <button id="btIdBms" className="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xl-4 btn-form" onClick={() => handleClickSelected(3)}>{t("formbms.btnbms")}</button>
                    </div>

                    <div className="card-form col-12 col-md-8 col-lg-8 col-xl-8">
                        <div id="formIdInstrum" style={{ display: "block" }}>
                            <FormInstr />
                        </div>
                        <div id="formIdHum" style={{ display: "none" }}>
                            <FormHum />
                        </div>
                        <div id="formIdBms" style={{ display: "none" }}>
                            <FormBMS />
                        </div>
                    </div>

                </div>
            </div>
            <Footer />
        </>
    )
}

export default Quote;