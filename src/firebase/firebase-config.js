import firebase from 'firebase';
import *  as firebaseui from "firebaseui"
//import config from '../../config';

const firebaseConfig = {
    apiKey: "AIzaSyBOXpShdHdXSStzn5TAijm0Iyg40mEwFbM",
    authDomain: "gyeproyectos-43407.firebaseapp.com",
    projectId: "gyeproyectos-43407",
    storageBucket: "gyeproyectos-43407.appspot.com",
    messagingSenderId: "1096748677093",
    appId: "1:1096748677093:web:2e8b6835c9cb59a069210c",
    measurementId: "G-XSK8SK864X"
};

const uiConfig = {
    signInOptions: [
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        // firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        // firebase.auth.GithubAuthProvider.PROVIDER_ID,
        // firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    signInSuccessUrl: '/',
};

firebase.initializeApp(firebaseConfig);


export const auth = firebase.auth();
export const db = firebase.firestore();

/*db.settings({
    timestampsInSnapshots: true,
});*/

export const startUi = (elementId) => {
    const ui = new firebaseui.auth.AuthUI(auth);
    ui.start(elementId, uiConfig);
};
